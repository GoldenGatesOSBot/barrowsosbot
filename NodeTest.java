import java.awt.Graphics;
import java.util.ArrayList;

import org.osbot.script.Script;
import org.osbot.script.ScriptManifest;

@ScriptManifest(author = "GoldenGates", info = "Node example", name = "Node Example", version = 1.0)
public class NodeTest extends Script {

	private ArrayList<Node> nodes = new ArrayList<Node>();

	public void onStart() {

		nodes.add(new NodeTestNodeClass(this));

	}

	public int onLoop() throws InterruptedException {
		for (Node n : nodes) {
			if (n.validate()) {
				n.execute();
			}
		}
		return 10;
	}

	public void onPaint(Graphics g1) {

	}
}
